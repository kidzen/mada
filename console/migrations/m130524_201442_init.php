<?php

use yii\db\Migration;

class m130524_201442_init extends Migration
{
    public $schemaName = 'mada';
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->down();
        $this->createTable('{{%user}}', [
            'id' => $this->primaryKey(),
            'remote_ip' => $this->string(),
            'mobile_id' => $this->string(),

            'status' => $this->smallInteger()->notNull()->defaultValue(10),
            'created_at' => $this->timestamp()->notNull(),
            'updated_at' => $this->timestamp()->notNull(),
            ], $tableOptions);

            // [$i++, 'Tempat Bertugas', 'Bahagian A: Maklumat Asas Responden',1],
            // [$i++, 'Jantina', 'Bahagian A: Maklumat Asas Responden',1],
            // [$i++, 'Kumpulan Gred', 'Bahagian A: Maklumat Asas Responden',1],
            // [$i++, 'Umur', 'Bahagian A: Maklumat Asas Responden',1],
            // [$i++, 'Tempoh Berkhidmat Di MADA', 'Bahagian A: Maklumat Asas Responden',1],

        $this->createTable('{{%personel_information}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(),
            'department' => $this->integer()->comment('Tempat Betugas'),
            'gender' => $this->integer()->comment('1 => male, 2 => female'),
            'age' => $this->integer()->comment('Umur'),
            'service_duration' => $this->integer()->comment('Tempoh Berkhidmat'),

            'status' => $this->smallInteger()->notNull()->defaultValue(10),
            'created_at' => $this->timestamp()->notNull(),
            'updated_at' => $this->timestamp()->notNull(),
            ], $tableOptions);

        $this->createTable('{{%section}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),

            'status' => $this->smallInteger()->notNull()->defaultValue(10),
            'created_at' => $this->timestamp(),
            'updated_at' => $this->timestamp(),
            'deleted_at' => $this->timestamp(),
            ], $tableOptions);

        $this->createTable('{{%session}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),

            'status' => $this->smallInteger()->notNull()->defaultValue(10),
            'created_at' => $this->timestamp(),
            'updated_at' => $this->timestamp(),
            'deleted_at' => $this->timestamp(),
            ], $tableOptions);

        $this->createTable('{{%session_jt}}', [
            'id' => $this->primaryKey(),
            'session_id' => $this->integer(),
            'question_id' => $this->integer(),

            'status' => $this->smallInteger()->notNull()->defaultValue(10),
            'created_at' => $this->timestamp(),
            'updated_at' => $this->timestamp(),
            'deleted_at' => $this->timestamp(),
            ], $tableOptions);

        $this->createTable('{{%question}}', [
            'id' => $this->primaryKey(),
            'type' => $this->smallInteger()->comment('
                Type : 1 => objective, 2 => subjective, 3 => secret question, 4 => personel info question'
            ),
            'section_id' => $this->integer(),
            'question' => $this->string(),

            'status' => $this->smallInteger()->notNull()->defaultValue(10),
            'created_at' => $this->timestamp(),
            'updated_at' => $this->timestamp(),
            'deleted_at' => $this->timestamp(),
            ], $tableOptions);

        $this->createTable('{{%response_lookup}}', [
            'id' => $this->primaryKey(),
            'question_id' => $this->integer()->notNull(),
            'response' => $this->string(),
            'positivity_level' => $this->smallInteger()->comment('Type : 1 => very bad, 2 => bad, 3 => good, 4 => very good, 5 => best'),

            'status' => $this->smallInteger()->notNull()->defaultValue(10),
            'created_at' => $this->timestamp(),
            'updated_at' => $this->timestamp(),
            'deleted_at' => $this->timestamp(),
            ], $tableOptions);

        $this->createTable('{{%response}}', [
            'id' => $this->primaryKey(),
            'session_id' => $this->integer(), //
            'user_id' => $this->integer()->notNull(),
            'question_id' => $this->integer()->notNull(),
            'response' => $this->string(),

            'status' => $this->smallInteger()->notNull()->defaultValue(10),
            'created_at' => $this->timestamp(),
            'updated_at' => $this->timestamp(),
            'deleted_at' => $this->timestamp(),
            ], $tableOptions);

        $j = 1;
        // $this->addForeignKey('fk'.$j++,'{{child_table}}','parent_id','{{parent_table}}','id');
        $this->addForeignKey('fk'.$j++,'{{response}}','user_id','{{user}}','id');
        $this->addForeignKey('fk'.$j++,'{{personel_information}}','user_id','{{user}}','id');
        $this->addForeignKey('fk'.$j++,'{{response}}','question_id','{{question}}','id');
        $this->addForeignKey('fk'.$j++,'{{response}}','session_id','{{session}}','id');
        $this->addForeignKey('fk'.$j++,'{{response_lookup}}','question_id','{{question}}','id');
        $this->addForeignKey('fk'.$j++,'{{question}}','section_id','{{section}}','id');
        $this->addForeignKey('fk'.$j++,'{{session_jt}}','question_id','{{question}}','id');
        $this->addForeignKey('fk'.$j++,'{{session_jt}}','session_id','{{session}}','id');

        $this->insertDefaultData();
        $this->insertScenario();
        $this->createCustomView();
    }

    protected function createCustomView() {

        $monthReportSql = "CREATE VIEW r_by_q_all AS
SELECT
 `session`
 ,`question`
 ,avg_response
 ,TRUNCATE(avg_response/4*100,2) percent
 , no_responden,qid,sid
FROM (
   SELECT
       s.id sid,q.id qid,ss.name SESSION,q.question,AVG(response-1) avg_response,COUNT(*) no_responden
   FROM response r
   LEFT JOIN SESSION ss ON ss.id = r.session_id
   LEFT JOIN question q ON q.id = r.question_id
   LEFT JOIN section s ON s.id = q.section_id
   LEFT JOIN session_jt sjt ON sjt.question_id = q.id AND sjt.session_id = ss.id
   GROUP BY ss.id,q.id
) t
 WHERE sid NOT IN (1,2,8)
 ORDER BY SESSION,avg_response DESC
;
";
        $monthReport = Yii::$app->db->createCommand($monthReportSql);
        $monthReport->execute();

        $yearReportSql = "";

        $yearReport = Yii::$app->db->createCommand($yearReportSql);
        $yearReport->execute();
    }

    public function insertDefaultData()
    {
        $i = 1;
        $this->batchInsert('{{%section}}', ['id','name'],
            [
            [$i++,'Bahagian Soalan Rahsia'],
            [$i++,'Bahagian A: Maklumat Asas Responden'],
            [$i++,'Bahagian B : Pengurusan Organisasi'],
            [$i++,'Bahagian C : Kepuasan Kerja'],
            [$i++,'Bahagian D : Kelengkapan Pejabat'],
            [$i++,'Bahagian E : Tekanan Di Tempat Kerja'],
            [$i++,'Bahagian F : Kemudahan - Kemudahan Lain'],
            [$i++,'Bahagian G : Pandangan Dan Cadangan'],
            ]
            );
        $i = 1;
        $this->batchInsert('{{%session}}', ['id','name'],
            [
            [$i++,'2016/1'],
            [$i++,'2016/2'],
            [$i++,'2017/1'],
            [$i++,'2017/2'],
            ]
            );
        $i = 1;
        $this->batchInsert('{{%session_jt}}', ['session_id','question_id'],
            [
                [1,$i++],
                [2,$i],
                [3,$i],
                [4,$i],

                // identity question
                [1,$i++],
                [2,$i],
                [3,$i],
                [4,$i],
                [1,$i++],
                [2,$i],
                [3,$i],
                [4,$i],
                [1,$i++],
                [2,$i],
                [3,$i],
                [4,$i],
                [1,$i++],
                [2,$i],
                [3,$i],
                [4,$i],

                // survey question
                [1,$i++],
                [2,$i],
                [3,$i],
                [4,$i],
                [1,$i++],
                [2,$i],
                [3,$i],
                [4,$i],
                [1,$i++],
                [2,$i],
                [3,$i],
                [4,$i],
                [1,$i++],
                [2,$i],
                [3,$i],
                [4,$i],
                [1,$i++],
                [2,$i],
                [3,$i],
                [4,$i],
                [1,$i++],
                [2,$i],
                [3,$i],
                [4,$i],
                [1,$i++],
                [2,$i],
                [3,$i],
                [4,$i],
                [1,$i++],
                [2,$i],
                [3,$i],
                [4,$i],
                [1,$i++],
                [2,$i],
                [3,$i],
                [4,$i],
                [1,$i++],
                [2,$i],
                [3,$i],
                [4,$i],
                [1,$i++],
                [2,$i],
                [3,$i],
                [4,$i],
                [1,$i++],
                [2,$i],
                [3,$i],
                [4,$i],
                [1,$i++],
                [2,$i],
                [3,$i],
                [4,$i],
                [1,$i++],
                [2,$i],
                [3,$i],
                [4,$i],
                [1,$i++],
                [2,$i],
                [3,$i],
                [4,$i],
                [1,$i++],
                [2,$i],
                [3,$i],
                [4,$i],
                [1,$i++],
                [2,$i],
                [3,$i],
                [4,$i],
                [1,$i++],
                [2,$i],
                [3,$i],
                [4,$i],
                [1,$i++],
                [2,$i],
                [3,$i],
                [4,$i],
                [1,$i++],
                [2,$i],
                [3,$i],
                [4,$i],
                [1,$i++],
                [2,$i],
                [3,$i],
                [4,$i],
                [1,$i++],
                [2,$i],
                [3,$i],
                [4,$i],
                [1,$i++],
                [2,$i],
                [3,$i],
                [4,$i],
                [1,$i++],
                [2,$i],
                [3,$i],
                [4,$i],
                [1,$i++],
                [2,$i],
                [3,$i],
                [4,$i],
                [1,$i++],
                [2,$i],
                [3,$i],
                [4,$i],
                [1,$i++],
                [2,$i],
                [3,$i],
                [4,$i],
            ]
            );
        // $i = 1;
        // $this->batchInsert('{{%session_jt}}', ['session_id','question_id'],
        //     [
        //         [2,$i++],

        //         // identity question
        //         [2,$i++],
        //         [2,$i++],
        //         [2,$i++],
        //         [2,$i++],

        //         // survey question
        //         [2,$i++],
        //         [2,$i++],
        //         [2,$i++],
        //         [2,$i++],
        //         [2,$i++],
        //         [2,$i++],
        //         [2,$i++],
        //         [2,$i++],
        //         [2,$i++],
        //         [2,$i++],
        //         [2,$i++],
        //         [2,$i++],
        //         [2,$i++],
        //         [2,$i++],
        //         [2,$i++],
        //         [2,$i++],
        //         [2,$i++],
        //         [2,$i++],
        //         [2,$i++],
        //         [2,$i++],
        //         [2,$i++],
        //         [2,$i++],
        //         [2,$i++],
        //         [2,$i++],
        //         [2,$i++],
        //         [2,$i++],
        //         [2,$i++],
        //     ]
        //     );
        $i = 1;
        $this->batchInsert('{{%question}}', ['id','question','section_id','type'],
            [
            // session 2017 1nd half
            // secret question
            [$i++, 'Adakah anda pekerja MADA', 1,3],

            // identity question
            [$i++, 'Tempat Bertugas', 2,4],
            [$i++, 'Jantina', 2,4],
            [$i++, 'Kumpulan Gred', 2,4],
            [$i++, 'Umur', 2,4],

            // survey question
            [$i++, 'Tempoh Berkhidmat Di MADA', 2,1],
            [$i++, 'Saya bersetuju dengan penempatan sekarang', 3,1],
            [$i++, 'Arahan penyelia adalah jelas dan mudah difahami', 3,1],
            [$i++, 'Pembahagian kerja dilakukan secara adil', 3,1],
            [$i++, 'Keperihatinan pengurusan atasan akan kebajikan dan permasalahan warga MADA', 3,1],
            [$i++, 'Bidang tugas saya sesuai dengan kelayakan saya', 3,1],
            [$i++, 'Peluang untuk melakukan kerja yang berbeza dari semasa ke semasa', 4,1],
            [$i++, 'Peluang untuk melakukan sesuatu yang menggunakan kebolehan dan kepakaran sendiri', 4,1],
            [$i++, 'Peluang untuk terus maju dan berjaya dalam kerjaya', 4,1],
            [$i++, 'Kebolehan pegawai atasan dalam membuat keputusan', 4,1],
            [$i++, 'Pegawai atasan memberikan galakan dan pujian', 4,1],
            [$i++, 'Ruang pejabat di bahagian / cawangan saya', 5,1],
            [$i++, 'Bilangan komputer di bahagian / cawangan saya', 5,1],
            [$i++, 'Bilangan pencetak di bahagian / cawangan saya', 5,1],
            [$i++, 'Keadaan perabot [meja, kerusi & lain-lain) di bahagian / cawangan saya', 5,1],
            [$i++, 'Keselesaan berkerja di bahagian / cawangan saya', 5,1],
            [$i++, 'Mempunyai hubungan yang baik dengan penyelia / ketua', 6,1],
            [$i++, 'Mempunyai hubungan yang baik sesama kakitangan yang lain', 6,1],
            [$i++, 'Amalan saling menghormati di antara satu sama lain tanpa mengira pangkat', 6,1],
            [$i++, 'Semangat kekitaan di kalangan warga MADA', 6,1],
            [$i++, 'Rakan sekerja sentiasa memberi dorongan dan galakan', 6,1],
            [$i++, 'Mempunyai hubungan yang baik dengan penyelia / ketua', 7,1],
            [$i++, 'Mempunyai hubungan yang baik sesama kakitangan yang lain', 7,1],
            [$i++, 'Amalan saling menghormati di antara satu sama lain tanpa mengira pangkat', 7,1],
            [$i++, 'Semangat kekitaan di kalangan warga MADA', 7,1],
            [$i++, 'Rakan sekerja sentiasa memberi dorongan dan galakan', 7,1],
            [$i++, 'Berikan pandangan anda tentang Diari MADA yang diterbitkan, dan nyatakan cadangan anda untuk penambahbaikan Diari Mada 2018 yang akan datang', 8,2],

            ]
            );
    }

    public function insertScenario()
    {
        //user registration

        //user response
        // $this->createTable('{{%response}}', [
        //     'id' => $this->primaryKey(),
        //     'user_id' => $this->integer()->notNull(),
        //     'question_id' => $this->integer()->notNull(),
        //     'response' => $this->string(),

        //     'status' => $this->smallInteger()->notNull()->defaultValue(10),
        //     'created_at' => $this->timestamp(),
        //     'updated_at' => $this->timestamp(),
        //     'deleted_at' => $this->timestamp(),
        //     ], $tableOptions);
        $ips = [];
        $ip = '192.'.rand(1,200).'.'.rand(1,99).'.'.rand(1,999);
        for ($u=1; $u < 1001 ; $u++) {
            while (array_key_exists($ip, $ips)) {
                # code...175.139.6.221
                $ip = rand(1,192).'.'.rand(1,168).'.'.rand(1,99).'.'.rand(1,300);
            }
            $ips[$ip] = $ip;

            $this->insert('{{%user}}', [
                'id' => $u,
                'remote_ip' => $ip,
                'mobile_id' => Yii::$app->security->generateRandomString(),
                ]);

            $q = 1;
            $s = 1;
            // session 1st
            $this->batchInsert('{{%response}}', ['user_id','session_id','question_id','response'],
                [
                    // secret question
                    // 'Adakah anda pekerja MADA', 1,3],
                    [$u,$s,$q++,rand(0,1)],

                     // 'Tempat Bertugas', 2,4],
                    [$u,$s,$q++,rand(1,5)],
                     // 'Jantina', 2,4],
                    [$u,$s,$q++,rand(1,2)],
                     // 'Kumpulan Gred', 2,4],
                    [$u,$s,$q++,rand(1,5)],
                     // 'Umur', 2,4],
                    [$u,$s,$q++,rand(18,60)],
                     // 'Tempoh Berkhidmat Di MADA', 2,1],
                    [$u,$s,$q++,rand(1,5)],

                    // survey question
                     // 'Saya bersetuju dengan penempatan sekarang', 3,1],
                    [$u,$s,$q++,rand(1,2)],
                     // 'Arahan penyelia adalah jelas dan mudah difahami', 3,1],
                    [$u,$s,$q++,rand(1,3)],
                     // 'Pembahagian kerja dilakukan secara adil', 3,1],
                    [$u,$s,$q++,rand(2,3)],
                     // 'Keperihatinan pengurusan atasan akan kebajikan dan permasalahan warga MADA', 3,1],
                    [$u,$s,$q++,rand(2,4)],
                     // 'Bidang tugas saya sesuai dengan kelayakan saya', 3,1],
                    [$u,$s,$q++,rand(3,4)],
                     // 'Peluang untuk melakukan kerja yang berbeza dari semasa ke semasa', 4,1],
                    [$u,$s,$q++,rand(3,5)],
                     // 'Peluang untuk melakukan sesuatu yang menggunakan kebolehan dan kepakaran sendiri', 4,1],
                    [$u,$s,$q++,rand(4,5)],
                     // 'Peluang untuk terus maju dan berjaya dalam kerjaya', 4,1],
                    [$u,$s,$q++,1],
                     // 'Kebolehan pegawai atasan dalam membuat keputusan', 4,1],
                    [$u,$s,$q++,2],
                     // 'Pegawai atasan memberikan galakan dan pujian', 4,1],
                    [$u,$s,$q++,3],
                     // 'Ruang pejabat di bahagian / cawangan saya', 5,1],
                    [$u,$s,$q++,4],
                     // 'Bilangan komputer di bahagian / cawangan saya', 5,1],
                    [$u,$s,$q++,5],
                     // 'Bilangan pencetak di bahagian / cawangan saya', 5,1],
                    [$u,$s,$q++,rand(1,rand(1,3))],
                     // 'Keadaan perabot [meja, kerusi & lain-lain) di bahagian / cawangan saya', 5,1],
                    [$u,$s,$q++,rand(1,rand(2,4))],
                     // 'Keselesaan berkerja di bahagian / cawangan saya', 5,1],
                    [$u,$s,$q++,rand(2,rand(2,4))],
                     // 'Mempunyai hubungan yang baik dengan penyelia / ketua', 6,1],
                    [$u,$s,$q++,rand(1,rand(3,5))],
                     // 'Mempunyai hubungan yang baik sesama kakitangan yang lain', 6,1],
                    [$u,$s,$q++,rand(rand(3,5),5)],
                     // 'Amalan saling menghormati di antara satu sama lain tanpa mengira pangkat', 6,1],
                    [$u,$s,$q++,rand(rand(2,4),5)],
                     // 'Semangat kekitaan di kalangan warga MADA', 6,1],
                    [$u,$s,$q++,rand(rand(2,4),4)],
                     // 'Rakan sekerja sentiasa memberi dorongan dan galakan', 6,1],
                    [$u,$s,$q++,rand(rand(1,3),5)],
                     // 'Mempunyai hubungan yang baik dengan penyelia / ketua', 7,1],
                    [$u,$s,$q++,rand(1,5)],
                     // 'Mempunyai hubungan yang baik sesama kakitangan yang lain', 7,1],
                    [$u,$s,$q++,rand(1,5)],
                     // 'Amalan saling menghormati di antara satu sama lain tanpa mengira pangkat', 7,1],
                    [$u,$s,$q++,rand(1,5)],
                     // 'Semangat kekitaan di kalangan warga MADA', 7,1],
                    [$u,$s,$q++,rand(1,5)],
                     // 'Rakan sekerja sentiasa memberi dorongan dan galakan', 7,1],
                    [$u,$s,$q++,rand(1,5)],
                     // 'Berikan pandangan anda tentang Diari MADA yang diterbitkan, dan nyatakan cadangan anda untuk penambahbaikan Diari Mada 2018 yang akan datang', 8,2],
                    [$u,$s,$q++,'response bertulis dari penguna : '.$u],

                ]
            );
            // session 2nd
            $q = 1;
            $s = 2;
            $this->batchInsert('{{%response}}', ['user_id','session_id','question_id','response'],
                [
                    // secret question
                    // 'Adakah anda pekerja MADA', 1,3],
                    [$u,$s,$q++,rand(1,5)],

                     // 'Tempat Bertugas', 2,4],
                    [$u,$s,$q++,rand(1,5)],
                     // 'Jantina', 2,4],
                    [$u,$s,$q++,rand(1,5)],
                     // 'Kumpulan Gred', 2,4],
                    [$u,$s,$q++,rand(1,5)],
                     // 'Umur', 2,4],
                    [$u,$s,$q++,rand(1,5)],
                     // 'Tempoh Berkhidmat Di MADA', 2,1],
                    [$u,$s,$q++,rand(1,5)],

                    // survey question
                     // 'Saya bersetuju dengan penempatan sekarang', 3,1],
                    [$u,$s,$q++,rand(1,5)],
                     // 'Arahan penyelia adalah jelas dan mudah difahami', 3,1],
                    [$u,$s,$q++,rand(1,2)],
                     // 'Pembahagian kerja dilakukan secara adil', 3,1],
                    [$u,$s,$q++,rand(1,3)],
                     // 'Keperihatinan pengurusan atasan akan kebajikan dan permasalahan warga MADA', 3,1],
                    [$u,$s,$q++,rand(2,3)],
                     // 'Bidang tugas saya sesuai dengan kelayakan saya', 3,1],
                    [$u,$s,$q++,rand(2,4)],
                     // 'Peluang untuk melakukan kerja yang berbeza dari semasa ke semasa', 4,1],
                    [$u,$s,$q++,rand(3,4)],
                     // 'Peluang untuk melakukan sesuatu yang menggunakan kebolehan dan kepakaran sendiri', 4,1],
                    [$u,$s,$q++,rand(3,5)],
                     // 'Peluang untuk terus maju dan berjaya dalam kerjaya', 4,1],
                    [$u,$s,$q++,rand(4,5)],
                     // 'Kebolehan pegawai atasan dalam membuat keputusan', 4,1],
                    [$u,$s,$q++,1],
                     // 'Pegawai atasan memberikan galakan dan pujian', 4,1],
                    [$u,$s,$q++,2],
                     // 'Ruang pejabat di bahagian / cawangan saya', 5,1],
                    [$u,$s,$q++,3],
                     // 'Bilangan komputer di bahagian / cawangan saya', 5,1],
                    [$u,$s,$q++,4],
                     // 'Bilangan pencetak di bahagian / cawangan saya', 5,1],
                    [$u,$s,$q++,5],
                     // 'Keadaan perabot [meja, kerusi & lain-lain) di bahagian / cawangan saya', 5,1],
                    [$u,$s,$q++,rand(1,rand(1,3))],
                     // 'Keselesaan berkerja di bahagian / cawangan saya', 5,1],
                    [$u,$s,$q++,rand(1,rand(2,4))],
                     // 'Mempunyai hubungan yang baik dengan penyelia / ketua', 6,1],
                    [$u,$s,$q++,rand(2,rand(2,4))],
                     // 'Mempunyai hubungan yang baik sesama kakitangan yang lain', 6,1],
                    [$u,$s,$q++,rand(1,rand(3,5))],
                     // 'Amalan saling menghormati di antara satu sama lain tanpa mengira pangkat', 6,1],
                    [$u,$s,$q++,rand(rand(3,5),5)],
                     // 'Semangat kekitaan di kalangan warga MADA', 6,1],
                    [$u,$s,$q++,rand(rand(2,4),5)],
                     // 'Rakan sekerja sentiasa memberi dorongan dan galakan', 6,1],
                    [$u,$s,$q++,rand(rand(2,4),4)],
                     // 'Mempunyai hubungan yang baik dengan penyelia / ketua', 7,1],
                    [$u,$s,$q++,rand(rand(1,3),5)],
                     // 'Mempunyai hubungan yang baik sesama kakitangan yang lain', 7,1],
                    [$u,$s,$q++,rand(1,5)],
                     // 'Amalan saling menghormati di antara satu sama lain tanpa mengira pangkat', 7,1],
                    [$u,$s,$q++,rand(1,5)],
                     // 'Semangat kekitaan di kalangan warga MADA', 7,1],
                    [$u,$s,$q++,rand(1,5)],
                     // 'Rakan sekerja sentiasa memberi dorongan dan galakan', 7,1],
                    [$u,$s,$q++,rand(1,5)],
                     // 'Berikan pandangan anda tentang Diari MADA yang diterbitkan, dan nyatakan cadangan anda untuk penambahbaikan Diari Mada 2018 yang akan datang', 8,2],
                    [$u,$s,$q++,'response bertulis dari penguna : '.$u],

                ]
            );
            // session 3rd
            $q = 1;
            $s = 3;
            $this->batchInsert('{{%response}}', ['user_id','session_id','question_id','response'],
                [
                    // secret question
                    // 'Adakah anda pekerja MADA', 1,3],
                    [$u,$s,$q++,rand(1,5)],

                     // 'Tempat Bertugas', 2,4],
                    [$u,$s,$q++,rand(1,5)],
                     // 'Jantina', 2,4],
                    [$u,$s,$q++,rand(1,5)],
                     // 'Kumpulan Gred', 2,4],
                    [$u,$s,$q++,rand(1,5)],
                     // 'Umur', 2,4],
                    [$u,$s,$q++,rand(1,5)],
                     // 'Tempoh Berkhidmat Di MADA', 2,1],
                    [$u,$s,$q++,rand(1,5)],

                    // survey question
                     // 'Saya bersetuju dengan penempatan sekarang', 3,1],
                    [$u,$s,$q++,rand(1,5)],
                     // 'Arahan penyelia adalah jelas dan mudah difahami', 3,1],
                    [$u,$s,$q++,rand(1,2)],
                     // 'Pembahagian kerja dilakukan secara adil', 3,1],
                    [$u,$s,$q++,rand(1,3)],
                     // 'Keperihatinan pengurusan atasan akan kebajikan dan permasalahan warga MADA', 3,1],
                    [$u,$s,$q++,rand(2,3)],
                     // 'Bidang tugas saya sesuai dengan kelayakan saya', 3,1],
                    [$u,$s,$q++,rand(2,4)],
                     // 'Peluang untuk melakukan kerja yang berbeza dari semasa ke semasa', 4,1],
                    [$u,$s,$q++,rand(3,4)],
                     // 'Peluang untuk melakukan sesuatu yang menggunakan kebolehan dan kepakaran sendiri', 4,1],
                    [$u,$s,$q++,rand(3,5)],
                     // 'Peluang untuk terus maju dan berjaya dalam kerjaya', 4,1],
                    [$u,$s,$q++,rand(4,5)],
                     // 'Kebolehan pegawai atasan dalam membuat keputusan', 4,1],
                    [$u,$s,$q++,1],
                     // 'Pegawai atasan memberikan galakan dan pujian', 4,1],
                    [$u,$s,$q++,2],
                     // 'Ruang pejabat di bahagian / cawangan saya', 5,1],
                    [$u,$s,$q++,3],
                     // 'Bilangan komputer di bahagian / cawangan saya', 5,1],
                    [$u,$s,$q++,4],
                     // 'Bilangan pencetak di bahagian / cawangan saya', 5,1],
                    [$u,$s,$q++,5],
                     // 'Keadaan perabot [meja, kerusi & lain-lain) di bahagian / cawangan saya', 5,1],
                    [$u,$s,$q++,rand(1,rand(1,3))],
                     // 'Keselesaan berkerja di bahagian / cawangan saya', 5,1],
                    [$u,$s,$q++,rand(1,rand(2,4))],
                     // 'Mempunyai hubungan yang baik dengan penyelia / ketua', 6,1],
                    [$u,$s,$q++,rand(2,rand(2,4))],
                     // 'Mempunyai hubungan yang baik sesama kakitangan yang lain', 6,1],
                    [$u,$s,$q++,rand(1,rand(3,5))],
                     // 'Amalan saling menghormati di antara satu sama lain tanpa mengira pangkat', 6,1],
                    [$u,$s,$q++,rand(rand(3,5),5)],
                     // 'Semangat kekitaan di kalangan warga MADA', 6,1],
                    [$u,$s,$q++,rand(rand(2,4),5)],
                     // 'Rakan sekerja sentiasa memberi dorongan dan galakan', 6,1],
                    [$u,$s,$q++,rand(rand(2,4),4)],
                     // 'Mempunyai hubungan yang baik dengan penyelia / ketua', 7,1],
                    [$u,$s,$q++,rand(rand(1,3),5)],
                     // 'Mempunyai hubungan yang baik sesama kakitangan yang lain', 7,1],
                    [$u,$s,$q++,rand(1,5)],
                     // 'Amalan saling menghormati di antara satu sama lain tanpa mengira pangkat', 7,1],
                    [$u,$s,$q++,rand(1,5)],
                     // 'Semangat kekitaan di kalangan warga MADA', 7,1],
                    [$u,$s,$q++,rand(1,5)],
                     // 'Rakan sekerja sentiasa memberi dorongan dan galakan', 7,1],
                    [$u,$s,$q++,rand(1,5)],
                     // 'Berikan pandangan anda tentang Diari MADA yang diterbitkan, dan nyatakan cadangan anda untuk penambahbaikan Diari Mada 2018 yang akan datang', 8,2],
                    [$u,$s,$q++,'response bertulis dari penguna : '.$u],

                ]
            );
            // session 4th
            $q = 1;
            $s = 4;
            $this->batchInsert('{{%response}}', ['user_id','session_id','question_id','response'],
                [
                    // secret question
                    // 'Adakah anda pekerja MADA', 1,3],
                    [$u,$s,$q++,rand(1,5)],

                     // 'Tempat Bertugas', 2,4],
                    [$u,$s,$q++,rand(1,5)],
                     // 'Jantina', 2,4],
                    [$u,$s,$q++,rand(1,5)],
                     // 'Kumpulan Gred', 2,4],
                    [$u,$s,$q++,rand(1,5)],
                     // 'Umur', 2,4],
                    [$u,$s,$q++,rand(1,5)],
                     // 'Tempoh Berkhidmat Di MADA', 2,1],
                    [$u,$s,$q++,rand(1,5)],

                    // survey question
                     // 'Saya bersetuju dengan penempatan sekarang', 3,1],
                    [$u,$s,$q++,rand(1,5)],
                     // 'Arahan penyelia adalah jelas dan mudah difahami', 3,1],
                    [$u,$s,$q++,rand(1,2)],
                     // 'Pembahagian kerja dilakukan secara adil', 3,1],
                    [$u,$s,$q++,rand(1,3)],
                     // 'Keperihatinan pengurusan atasan akan kebajikan dan permasalahan warga MADA', 3,1],
                    [$u,$s,$q++,rand(2,3)],
                     // 'Bidang tugas saya sesuai dengan kelayakan saya', 3,1],
                    [$u,$s,$q++,rand(2,4)],
                     // 'Peluang untuk melakukan kerja yang berbeza dari semasa ke semasa', 4,1],
                    [$u,$s,$q++,rand(3,4)],
                     // 'Peluang untuk melakukan sesuatu yang menggunakan kebolehan dan kepakaran sendiri', 4,1],
                    [$u,$s,$q++,rand(3,5)],
                     // 'Peluang untuk terus maju dan berjaya dalam kerjaya', 4,1],
                    [$u,$s,$q++,rand(4,5)],
                     // 'Kebolehan pegawai atasan dalam membuat keputusan', 4,1],
                    [$u,$s,$q++,1],
                     // 'Pegawai atasan memberikan galakan dan pujian', 4,1],
                    [$u,$s,$q++,2],
                     // 'Ruang pejabat di bahagian / cawangan saya', 5,1],
                    [$u,$s,$q++,3],
                     // 'Bilangan komputer di bahagian / cawangan saya', 5,1],
                    [$u,$s,$q++,4],
                     // 'Bilangan pencetak di bahagian / cawangan saya', 5,1],
                    [$u,$s,$q++,5],
                     // 'Keadaan perabot [meja, kerusi & lain-lain) di bahagian / cawangan saya', 5,1],
                    [$u,$s,$q++,rand(1,rand(1,3))],
                     // 'Keselesaan berkerja di bahagian / cawangan saya', 5,1],
                    [$u,$s,$q++,rand(1,rand(2,4))],
                     // 'Mempunyai hubungan yang baik dengan penyelia / ketua', 6,1],
                    [$u,$s,$q++,rand(2,rand(2,4))],
                     // 'Mempunyai hubungan yang baik sesama kakitangan yang lain', 6,1],
                    [$u,$s,$q++,rand(1,rand(3,5))],
                     // 'Amalan saling menghormati di antara satu sama lain tanpa mengira pangkat', 6,1],
                    [$u,$s,$q++,rand(rand(3,5),5)],
                     // 'Semangat kekitaan di kalangan warga MADA', 6,1],
                    [$u,$s,$q++,rand(rand(2,4),5)],
                     // 'Rakan sekerja sentiasa memberi dorongan dan galakan', 6,1],
                    [$u,$s,$q++,rand(rand(2,4),4)],
                     // 'Mempunyai hubungan yang baik dengan penyelia / ketua', 7,1],
                    [$u,$s,$q++,rand(rand(1,3),5)],
                     // 'Mempunyai hubungan yang baik sesama kakitangan yang lain', 7,1],
                    [$u,$s,$q++,rand(1,5)],
                     // 'Amalan saling menghormati di antara satu sama lain tanpa mengira pangkat', 7,1],
                    [$u,$s,$q++,rand(1,5)],
                     // 'Semangat kekitaan di kalangan warga MADA', 7,1],
                    [$u,$s,$q++,rand(1,5)],
                     // 'Rakan sekerja sentiasa memberi dorongan dan galakan', 7,1],
                    [$u,$s,$q++,rand(1,5)],
                     // 'Berikan pandangan anda tentang Diari MADA yang diterbitkan, dan nyatakan cadangan anda untuk penambahbaikan Diari Mada 2018 yang akan datang', 8,2],
                    [$u,$s,$q++,'response bertulis dari penguna : '.$u],

                ]
            );
        }
    }
    //drop function
    public function down()
    {
            //drop for confirm
        try {
                //set fk check off
            $fkCheckOff = "SET FOREIGN_KEY_CHECKS = 0";
            $sqlObject = Yii::$app->db->createCommand($fkCheckOff)->execute();
                //prepare script for dropping table and view
            $script = "SELECT concat('DROP TABLE IF EXISTS ', table_name, ';') as query
            FROM information_schema.tables
            WHERE table_schema = '".$this->schemaName."' and table_name != 'migration'";

            $viewDropScript = "SELECT CONCAT('DROP VIEW IF EXISTS ', table_name, ';') AS query
            FROM information_schema.tables
            WHERE table_schema = '".$this->schemaName."' AND TABLE_TYPE LIKE 'VIEW'";

                //dropping view
            $viewDropObject = Yii::$app->db->createCommand($viewDropScript)->queryAll();
            foreach ($viewDropObject as $viewQuery) {
                var_dump($viewQuery['query']);
                $command = Yii::$app->db->createCommand($viewQuery['query']);
                $command->execute();
            }

                //dropping table
            $sqlObject = Yii::$app->db->createCommand($script)->queryAll();
            foreach ($sqlObject as $query) {
                var_dump($query['query']);
                $command = Yii::$app->db->createCommand($query['query']);
                $command->execute();
            }

                //set fk check back on
            $fkCheckOn = "SET FOREIGN_KEY_CHECKS = 1";
            $sqlObject = Yii::$app->db->createCommand($fkCheckOff)->execute();

        } catch (\yii\db\Exception $e) {
            var_dump($e);
        }
    }
}
