<?php

namespace common\components\helper;

use Yii;

/**
 * This is the model class for table "cases".
 */
class Helper
{

    public static function RomanConverter($integer, $upcase = false, $reverse = false)
    {
        if (!$reverse) {
            $table = array('M'=>1000, 'CM'=>900, 'D'=>500, 'CD'=>400, 'C'=>100, 'XC'=>90, 'L'=>50, 'XL'=>40, 'X'=>10, 'IX'=>9, 'V'=>5, 'IV'=>4, 'I'=>1);
            $return = '';
            while($integer > 0)
            {
                foreach($table as $rom=>$arb)
                {
                    if($integer >= $arb)
                    {
                        $integer -= $arb;
                        $return .= $rom;
                        break;
                    }
                }
            }
            if(!$upcase) {
                $return = strtolower($return);
            }
        }

        return $return;
    }
}



