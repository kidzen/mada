<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace common\components;

class User extends \yii\web\User {

    public function getStatus() {
        $identity = $this->getIdentity();

        return $identity !== null ? 'Online' : 'Offline';
    }
    public function getAvatar() {
        $identity = $this->getIdentity();

        return $identity !== null ? $identity->getAvatar() : null;
    }
//    public function getStaffDetail() {
//        $identity = $this->getIdentity();
//
//        return $identity !== null ? $identity->getStaffDetails() : null;
//    }
    public function getUsername() {
        $identity = $this->getIdentity();

        return $identity !== null ? $identity->getUsername() : null;
    }
    public function getName() {
        $identity = $this->getIdentity();
        return $identity !== null ? $identity->getName() : null;
    }

    public function getAccess() {
        $identity = $this->getIdentity();

        return $identity !== null ? $identity->role->name : null;
    }

    public function getIsDev() {
        $identity = $this->getAccess();

        return $identity === 'Developer' ? true : false;
    }

    public function getIsAdmin() {
        $identity = $this->getAccess();

        return $identity === 'Super Admin' ? true : false;
    }

    public function getIsAdminJuu() {
        $identity = $this->getAccess();

        return $identity === 'Admin Jabatan Undang-Undang' ? true : false;
    }

    public function getIsUserJuu() {
        $identity = $this->getAccess();

        return $identity === 'User Jabatan Undang-Undang' ? true : false;
    }

    public function getIsAdminDepartment() {
        $identity = $this->getAccess();

        return $identity === 'Admin Jabatan' ? true : false;
    }

    public function getIsUserDepartment() {
        $identity = $this->getAccess();

        return $identity === 'User Jabatan' ? true : false;
    }

    public function getIsContractor() {
        $identity = $this->getAccess();

        return $identity === 'Kontraktor' ? true : false;
    }

    public function getIsUser() {
        $identity = $this->getAccess();

        return $identity === 'Common User' ? true : false;
    }

    public function getIsApprover() {
        $identity = $this->getAccess();

        return $identity === 'Approver' ? true : false;
    }

    public function getIsManager() {
        $identity = $this->getAccess();

        return $identity === 'Manager' ? true : false;
    }

    public function getIsJurugegas() {
        $identity = $this->getAccess();

        return $identity === 'Jurugegas' ? true : false;
    }

    public function getIsGuest() {
        $identity = $this->getAccess();

        return ($identity === 'Common User' || $identity === null) ? true : false;
    }

//    public function getIdentity($autoRenew = true) {
//        if ($this->_identity === false) {
//            if ($this->enableSession && $autoRenew) {
//                $this->_identity = null;
//                $this->renewAuthStatus();
//            } else {
//                return null;
//            }
//        }
//
//        return $this->_identity;
//    }
}
