<?php

namespace common\models;

use Yii;
use \common\models\base\ChildTable as BaseChildTable;

/**
 * This is the model class for table "child_table".
 */
class ChildTable extends BaseChildTable
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['parent_id'], 'required'],
            [['parent_id', 'status'], 'integer'],
            [['created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['name', 'description'], 'string', 'max' => 255]
        ]);
    }
	
}
