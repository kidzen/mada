<?php

namespace common\models\query;

/**
 * This is the ActiveQuery class for [[\common\models\query\RByQAll]].
 *
 * @see \common\models\query\RByQAll
 */
class RByQAllQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return \common\models\query\RByQAll[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \common\models\query\RByQAll|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
