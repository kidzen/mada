<?php

namespace common\models\query;

/**
 * This is the ActiveQuery class for [[\common\models\query\SessionJt]].
 *
 * @see \common\models\query\SessionJt
 */
class SessionJtQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return \common\models\query\SessionJt[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \common\models\query\SessionJt|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
