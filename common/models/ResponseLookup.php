<?php

namespace common\models;

use Yii;
use \common\models\base\ResponseLookup as BaseResponseLookup;

/**
 * This is the model class for table "response_lookup".
 */
class ResponseLookup extends BaseResponseLookup
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['question_id'], 'required'],
            [['question_id', 'positivity_level', 'status'], 'integer'],
            [['created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['response'], 'string', 'max' => 255]
        ]);
    }
	
}
