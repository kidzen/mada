<?php

namespace common\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the base model class for table "question".
 *
 * @property integer $id
 * @property integer $type
 * @property integer $section_id
 * @property string $question
 * @property integer $status
 * @property string $created_at
 * @property string $updated_at
 * @property string $deleted_at
 *
 * @property \common\models\Section $section
 * @property \common\models\Response[] $responses
 * @property \common\models\ResponseLookup[] $responseLookups
 * @property \common\models\SessionJt[] $sessionJts
 */
class Question extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;


    /**
    * This function helps \mootensai\relation\RelationTrait runs faster
    * @return array relation names of this model
    */
    public function relationNames()
    {
        return [
            'section',
            'responses',
            'responseLookups',
            'sessionJts'
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type', 'section_id', 'status'], 'integer'],
            [['created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['question'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'question';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type' => 'Type',
            'section_id' => 'Section ID',
            'question' => 'Question',
            'status' => 'Status',
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSection()
    {
        return $this->hasOne(\common\models\Section::className(), ['id' => 'section_id'])->inverseOf('questions');
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getResponses()
    {
        return $this->hasMany(\common\models\Response::className(), ['question_id' => 'id'])->inverseOf('question');
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getResponseLookups()
    {
        return $this->hasMany(\common\models\ResponseLookup::className(), ['question_id' => 'id'])->inverseOf('question');
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSessionJts()
    {
        return $this->hasMany(\common\models\SessionJt::className(), ['question_id' => 'id'])->inverseOf('question');
    }
    
    /**
     * @inheritdoc
     * @return array mixed
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('CURRENT_TIMESTAMP()'),
            ],
        ];
    }


    /**
     * @inheritdoc
     * @return \common\models\query\QuestionQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\query\QuestionQuery(get_called_class());
    }
}
