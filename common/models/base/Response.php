<?php

namespace common\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the base model class for table "response".
 *
 * @property integer $id
 * @property integer $session_id
 * @property integer $user_id
 * @property integer $question_id
 * @property string $response
 * @property integer $status
 * @property string $created_at
 * @property string $updated_at
 * @property string $deleted_at
 *
 * @property \common\models\User $user
 * @property \common\models\Question $question
 * @property \common\models\Session $session
 */
class Response extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;


    /**
    * This function helps \mootensai\relation\RelationTrait runs faster
    * @return array relation names of this model
    */
    public function relationNames()
    {
        return [
            'user',
            'question',
            'session'
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['session_id', 'user_id', 'question_id', 'status'], 'integer'],
            [['user_id', 'question_id'], 'required'],
            [['created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['response'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'response';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'session_id' => 'Session ID',
            'user_id' => 'User ID',
            'question_id' => 'Question ID',
            'response' => 'Response',
            'status' => 'Status',
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(\common\models\User::className(), ['id' => 'user_id'])->inverseOf('responses');
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getQuestion()
    {
        return $this->hasOne(\common\models\Question::className(), ['id' => 'question_id'])->inverseOf('responses');
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSession()
    {
        return $this->hasOne(\common\models\Session::className(), ['id' => 'session_id'])->inverseOf('responses');
    }
    
    /**
     * @inheritdoc
     * @return array mixed
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
                'value' => new \yii\db\Expression('CURRENT_TIMESTAMP()'),
            ],
        ];
    }


    /**
     * @inheritdoc
     * @return \common\models\query\ResponseQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\query\ResponseQuery(get_called_class());
    }
}
