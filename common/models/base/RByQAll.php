<?php

namespace common\models\base;

use Yii;

/**
 * This is the base model class for table "r_by_q_all".
 *
 * @property string $session
 * @property string $question
 * @property double $avg_response
 * @property double $percent
 * @property integer $no_responden
 * @property integer $qid
 * @property integer $sid
 */
class RByQAll extends \yii\db\ActiveRecord
{
    use \mootensai\relation\RelationTrait;


    /**
    * This function helps \mootensai\relation\RelationTrait runs faster
    * @return array relation names of this model
    */
    public function relationNames()
    {
        return [
            ''
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['avg_response', 'percent'], 'number'],
            [['no_responden', 'qid', 'sid'], 'integer'],
            [['session', 'question'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'r_by_q_all';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'session' => 'Session',
            'question' => 'Question',
            'avg_response' => 'Avg Response',
            'percent' => 'Percent',
            'no_responden' => 'No Responden',
            'qid' => 'Qid',
            'sid' => 'Sid',
        ];
    }


    /**
     * @inheritdoc
     * @return \common\models\query\RByQAllQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\query\RByQAllQuery(get_called_class());
    }
}
