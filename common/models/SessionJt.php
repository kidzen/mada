<?php

namespace common\models;

use Yii;
use \common\models\base\SessionJt as BaseSessionJt;

/**
 * This is the model class for table "session_jt".
 */
class SessionJt extends BaseSessionJt
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['session_id', 'question_id', 'status'], 'integer'],
            [['created_at', 'updated_at', 'deleted_at'], 'safe']
        ]);
    }
	
}
