<?php

namespace common\models;

use Yii;
use \common\models\custom\User as BaseUser;

/**
 * This is the model class for table "user".
 */
class User extends BaseUser
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['status'], 'integer'],
            [['created_at', 'updated_at'], 'required'],
            [['created_at', 'updated_at'], 'safe'],
            [['remote_ip', 'mobile_id'], 'string', 'max' => 255]
        ]);
    }

}
