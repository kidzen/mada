<?php

namespace common\models;

use Yii;
use \common\models\base\RByQAll as BaseRByQAll;

/**
 * This is the model class for table "r_by_q_all".
 */
class RByQAll extends BaseRByQAll
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['avg_response', 'percent'], 'number'],
            [['no_responden', 'qid', 'sid'], 'integer'],
            [['session', 'question'], 'string', 'max' => 255]
        ]);
    }
	
}
