<?php

namespace common\models;

use Yii;
use \common\models\base\ParentTable as BaseParentTable;

/**
 * This is the model class for table "parent_table".
 */
class ParentTable extends BaseParentTable
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['status'], 'integer'],
            [['created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['name', 'description'], 'string', 'max' => 255]
        ]);
    }
	
}
