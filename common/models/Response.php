<?php

namespace common\models;

use Yii;
use \common\models\base\Response as BaseResponse;

/**
 * This is the model class for table "response".
 */
class Response extends BaseResponse
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['session_id', 'user_id', 'question_id', 'status'], 'integer'],
            [['user_id', 'question_id'], 'required'],
            [['created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['response'], 'string', 'max' => 255]
        ]);
    }
	
}
