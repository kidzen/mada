<?php

namespace common\models;

use Yii;
use \common\models\base\Question as BaseQuestion;

/**
 * This is the model class for table "question".
 */
class Question extends BaseQuestion
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['type', 'section_id', 'status'], 'integer'],
            [['created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['question'], 'string', 'max' => 255]
        ]);
    }
	
}
