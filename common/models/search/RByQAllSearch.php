<?php

namespace common\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\RByQAll;

/**
 * common\models\search\RByQAllSearch represents the model behind the search form about `common\models\RByQAll`.
 */
 class RByQAllSearch extends RByQAll
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['session', 'question'], 'safe'],
            [['avg_response', 'percent'], 'number'],
            [['no_responden', 'qid', 'sid'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = RByQAll::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'avg_response' => $this->avg_response,
            'percent' => $this->percent,
            'no_responden' => $this->no_responden,
            'qid' => $this->qid,
            'sid' => $this->sid,
        ]);

        $query->andFilterWhere(['like', 'session', $this->session])
            ->andFilterWhere(['like', 'question', $this->question]);

        return $dataProvider;
    }
}
