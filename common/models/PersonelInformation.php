<?php

namespace common\models;

use Yii;
use \common\models\base\PersonelInformation as BasePersonelInformation;

/**
 * This is the model class for table "personel_information".
 */
class PersonelInformation extends BasePersonelInformation
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['user_id', 'department', 'gender', 'age', 'service_duration', 'status'], 'integer'],
            [['created_at', 'updated_at'], 'required'],
            [['created_at', 'updated_at'], 'safe']
        ]);
    }
	
}
