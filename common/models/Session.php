<?php

namespace common\models;

use Yii;
use \common\models\base\Session as BaseSession;

/**
 * This is the model class for table "session".
 */
class Session extends BaseSession
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['status'], 'integer'],
            [['created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['name'], 'string', 'max' => 255]
        ]);
    }
	
}
