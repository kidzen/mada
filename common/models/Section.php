<?php

namespace common\models;

use Yii;
use \common\models\base\Section as BaseSection;

/**
 * This is the model class for table "section".
 */
class Section extends BaseSection
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['status'], 'integer'],
            [['created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['name'], 'string', 'max' => 255]
        ]);
    }
	
}
