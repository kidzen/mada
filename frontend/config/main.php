<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-frontend',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'frontend\controllers',
    'components' => [
        'view' => [
            'theme' => [
                'pathMap' => [
                    '@frontend/views' => '@frontend/themes/adminlte/views',
                ],
            ],
        ],
        'notify' => [
            'class' => 'common\components\Notify',
        ],
        'request' => [
            'csrfParam' => '_csrf-frontend',
        ],
        'user' => [
            'identityClass' => 'common\models\identity\User',
            'class' => 'common\components\User',
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity-frontend', 'httpOnly' => true],
        ],
        'session' => [
            // this is the name of the session cookie used for login on the frontend
            'name' => 'mada-frontend',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        /*
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
            ],
        ],
        */
    ],
    'modules' => [
        // 'datecontrol' => [
        //     'class' => '\kartik\datecontrol\Module',
        // ],
        // If you use tree table
        // 'treemanager' =>  [
        //     'class' => '\kartik\tree\Module',
        // ],
        'gridview' => [
            'class' => '\kartik\grid\Module',
        ],
        // 'debug' => [
        //     'class' => 'yii\debug\Module',
        //    'allowedIPs' => ['127.0.0.1', '::1', '192.168.0.*', '192.168.178.20','*'],
        // ],
        // 'webshell' => [
        //     'class' => 'samdark\webshell\Module',
        //     'yiiScript' => Yii::getAlias('@root'). '/yii', // adjust path to point to your ./yii script
        //     // 'allowedIPs' => ['127.0.0.1', '::1', '192.168.0.2'],
        //     // 'checkAccessCallback' => function (\yii\base\Action $action) {
        //         // return true if access is granted or false otherwise
        //     //     return true;
        //     // }
        // ],
    ],
    'params' => $params,
];
