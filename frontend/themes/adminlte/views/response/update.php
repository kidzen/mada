<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Response */

$this->title = 'Update Response: ' . ' ' . $model->response;
$this->params['breadcrumbs'][] = ['label' => 'Response', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->response, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="response-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
