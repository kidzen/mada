<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model common\models\Response */

$this->title = $model->response;
$this->params['breadcrumbs'][] = ['label' => 'Response', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="response-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= 'Response'.' '. Html::encode($this->title) ?></h2>
        </div>
        <div class="col-sm-3" style="margin-top: 15px">
            
            <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Delete', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ])
            ?>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        [
            'attribute' => 'session.name',
            'label' => 'Session',
        ],
        [
            'attribute' => 'user.id',
            'label' => 'User',
        ],
        [
            'attribute' => 'question.question',
            'label' => 'Question',
        ],
        'response',
        'status',
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]);
?>
    </div>
    <div class="row">
        <h4>User<?= ' '. Html::encode($this->title) ?></h4>
    </div>
    <?php 
    $gridColumnUser = [
        ['attribute' => 'id', 'visible' => false],
        'remote_ip',
        'mobile_id',
        'status',
    ];
    echo DetailView::widget([
        'model' => $model->user,
        'attributes' => $gridColumnUser    ]);
    ?>
    <div class="row">
        <h4>Question<?= ' '. Html::encode($this->title) ?></h4>
    </div>
    <?php 
    $gridColumnQuestion = [
        ['attribute' => 'id', 'visible' => false],
        'type',
        'section_id',
        'question',
        'status',
    ];
    echo DetailView::widget([
        'model' => $model->question,
        'attributes' => $gridColumnQuestion    ]);
    ?>
    <div class="row">
        <h4>Session<?= ' '. Html::encode($this->title) ?></h4>
    </div>
    <?php 
    $gridColumnSession = [
        ['attribute' => 'id', 'visible' => false],
        'name',
        'status',
    ];
    echo DetailView::widget([
        'model' => $model->session,
        'attributes' => $gridColumnSession    ]);
    ?>
</div>
