<aside class="main-sidebar">

    <section class="sidebar">

        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="<?= $directoryAsset ?>/img/user2-160x160.jpg" class="img-circle" alt="User Image"/>
            </div>
            <div class="pull-left info">
                <p>Alexander Pierce</p>

                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>

        <!-- search form -->
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search..."/>
              <span class="input-group-btn">
                <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
            </div>
        </form>
        <!-- /.search form -->

        <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu'],
                'items' => [
                    ['label' => 'Menu Yii2', 'options' => ['class' => 'header']],
                    // ['label' => 'Gii', 'icon' => 'file-code-o', 'url' => ['/gii']],
                    // ['label' => 'Debug', 'icon' => 'dashboard', 'url' => ['/debug']],
                    ['label' => 'Login', 'url' => ['site/login'], 'visible' => Yii::$app->user->isGuest],
                    [
                        'label' => 'Pentadbiran',
                        'items' => [
                            ['label' => 'Pengguna', 'url' => ['/user/index']],
                            ['label' => 'Pengurusan Soal Selidik', 'url' => ['/question/index']],
                            ['label' => 'Pengurusan Soalan', 'url' => ['/session-jt/index']],
                            ['label' => 'Pengurusan Sesi', 'url' => ['/session/index']],
                        ],
                    ],
                    [
                        'label' => 'Same tools',
                        // 'visible' => false,
                        'icon' => 'share',
                        'url' => '#',
                        'items' => [
                            [
                                'label' => 'Database',
                                // 'visible' => false,
                                'items' => [
                                    ['label' => 'Users', 'url' => ['/user/index']],
                                    ['label' => 'Personal Information', 'url' => ['/personel-information/index']],
                                    ['label' => 'Section', 'url' => ['/section/index']],
                                    ['label' => 'Question', 'url' => ['/question/index']],
                                    ['label' => 'Response Lookup', 'url' => ['/response-lookup/index']],
                                    ['label' => 'Response', 'url' => ['/response/index']],
                                ],
                            ],
                            ['label' => 'Gii', 'icon' => 'file-code-o', 'url' => ['/gii'],],
                            ['label' => 'Log', 'icon' => 'dashboard', 'url' => ['/debug'],],
                        ],
                    ],
                ],
            ]
        ) ?>

    </section>

</aside>
