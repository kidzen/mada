<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\RByQAll */

$this->title = 'Update Rby Qall: ' . ' ' . $model->session;
$this->params['breadcrumbs'][] = ['label' => 'Rby Qall', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->session, 'url' => ['view', ]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="rby-qall-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
