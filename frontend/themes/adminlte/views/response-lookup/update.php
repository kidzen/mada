<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\ResponseLookup */

$this->title = 'Update Response Lookup: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Response Lookup', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="response-lookup-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
