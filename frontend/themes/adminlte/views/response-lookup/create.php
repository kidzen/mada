<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\ResponseLookup */

$this->title = 'Create Response Lookup';
$this->params['breadcrumbs'][] = ['label' => 'Response Lookup', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="response-lookup-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
