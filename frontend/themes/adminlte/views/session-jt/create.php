<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\SessionJt */

$this->title = 'Create Session Jt';
$this->params['breadcrumbs'][] = ['label' => 'Session Jt', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="session-jt-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
