<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model common\models\ChildTable */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Child Table', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="child-table-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= 'Child Table'.' '. Html::encode($this->title) ?></h2>
        </div>
        <div class="col-sm-3" style="margin-top: 15px">
            
            <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Delete', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ])
            ?>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        [
            'attribute' => 'parent.name',
            'label' => 'Parent',
        ],
        'name',
        'description',
        'status',
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]);
?>
    </div>
    <div class="row">
        <h4>ParentTable<?= ' '. Html::encode($this->title) ?></h4>
    </div>
    <?php 
    $gridColumnParentTable = [
        ['attribute' => 'id', 'visible' => false],
        'name',
        'description',
        'status',
    ];
    echo DetailView::widget([
        'model' => $model->parent,
        'attributes' => $gridColumnParentTable    ]);
    ?>
</div>
