<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model common\models\Question */

?>
<div class="question-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= Html::encode($model->question) ?></h2>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        'type',
        [
            'attribute' => 'section.name',
            'label' => 'Section',
        ],
        'question',
        'status',
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>
</div>