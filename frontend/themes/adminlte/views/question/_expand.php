<?php
use yii\helpers\Html;
use kartik\tabs\TabsX;
use yii\helpers\Url;
$items = [
    [
        'label' => '<i class="glyphicon glyphicon-book"></i> '. Html::encode('Question'),
        'content' => $this->render('_detail', [
            'model' => $model,
        ]),
    ],
    //         [
    //     'label' => '<i class="glyphicon glyphicon-book"></i> '. Html::encode('Response'),
    //     'content' => $this->render('_dataResponse', [
    //         'model' => $model,
    //         'row' => $model->responses,
    //     ]),
    // ],
    //         [
    //     'label' => '<i class="glyphicon glyphicon-book"></i> '. Html::encode('Response Lookup'),
    //     'content' => $this->render('_dataResponseLookup', [
    //         'model' => $model,
    //         'row' => $model->responseLookups,
    //     ]),
    // ],
            [
        'label' => '<i class="glyphicon glyphicon-book"></i> '. Html::encode('Session Jt'),
        'content' => $this->render('_dataSessionJt', [
            'model' => $model,
            'row' => $model->sessionJts,
        ]),
    ],
    ];
echo TabsX::widget([
    'items' => $items,
    'position' => TabsX::POS_ABOVE,
    'encodeLabels' => false,
    'class' => 'tes',
    'pluginOptions' => [
        'bordered' => true,
        'sideways' => true,
        'enableCache' => false
    ],
]);
?>
