<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\PersonelInformation */

$this->title = 'Create Personel Information';
$this->params['breadcrumbs'][] = ['label' => 'Personel Information', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="personel-information-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
