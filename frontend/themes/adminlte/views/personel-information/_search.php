<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\search\PersonelInformationSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="form-personel-information-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

    <?= $form->field($model, 'user_id')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\common\models\User::find()->orderBy('id')->asArray()->all(), 'id', 'id'),
        'options' => ['placeholder' => 'Choose User'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>

    <?= $form->field($model, 'department')->textInput(['placeholder' => 'Department']) ?>

    <?= $form->field($model, 'gender')->textInput(['placeholder' => 'Gender']) ?>

    <?= $form->field($model, 'age')->textInput(['placeholder' => 'Age']) ?>

    <?php /* echo $form->field($model, 'service_duration')->textInput(['placeholder' => 'Service Duration']) */ ?>

    <?php /* echo $form->field($model, 'status')->dropDownList([0=>'Inactive',10=>'Active']) */ ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
