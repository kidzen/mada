<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\search\RByQAllSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="form-rby-qall-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'session')->textInput(['maxlength' => true, 'placeholder' => 'Session']) ?>

    <?= $form->field($model, 'question')->textInput(['maxlength' => true, 'placeholder' => 'Question']) ?>

    <?= $form->field($model, 'avg_response')->textInput(['placeholder' => 'Avg Response']) ?>

    <?= $form->field($model, 'percent')->textInput(['placeholder' => 'Percent']) ?>

    <?= $form->field($model, 'no_responden')->textInput(['placeholder' => 'No Responden']) ?>

    <?php /* echo $form->field($model, 'qid')->textInput(['placeholder' => 'Qid']) */ ?>

    <?php /* echo $form->field($model, 'sid')->textInput(['placeholder' => 'Sid']) */ ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
