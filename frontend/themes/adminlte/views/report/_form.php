<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\RByQAll */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="rby-qall-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->errorSummary($model); ?>

    <?= $form->field($model, 'session')->textInput(['maxlength' => true, 'placeholder' => 'Session']) ?>

    <?= $form->field($model, 'question')->textInput(['maxlength' => true, 'placeholder' => 'Question']) ?>

    <?= $form->field($model, 'avg_response')->textInput(['placeholder' => 'Avg Response']) ?>

    <?= $form->field($model, 'percent')->textInput(['placeholder' => 'Percent']) ?>

    <?= $form->field($model, 'no_responden')->textInput(['placeholder' => 'No Responden']) ?>

    <?= $form->field($model, 'qid')->textInput(['placeholder' => 'Qid']) ?>

    <?= $form->field($model, 'sid')->textInput(['placeholder' => 'Sid']) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
