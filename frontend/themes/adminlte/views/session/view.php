<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model common\models\Session */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Session', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="session-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= 'Session'.' '. Html::encode($this->title) ?></h2>
        </div>
        <div class="col-sm-3" style="margin-top: 15px">
            
            <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Delete', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ])
            ?>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        'name',
        'status',
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]);
?>
    </div>
    
    <div class="row">
<?php
if($providerResponse->totalCount){
    $gridColumnResponse = [
        ['class' => 'yii\grid\SerialColumn'],
            ['attribute' => 'id', 'visible' => false],
                        [
                'attribute' => 'user.id',
                'label' => 'User'
            ],
            [
                'attribute' => 'question.question',
                'label' => 'Question'
            ],
            'response',
            'status',
    ];
    echo Gridview::widget([
        'dataProvider' => $providerResponse,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-response']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Response'),
        ],
        'export' => false,
        'columns' => $gridColumnResponse
    ]);
}
?>

    </div>
    
    <div class="row">
<?php
if($providerSessionJt->totalCount){
    $gridColumnSessionJt = [
        ['class' => 'yii\grid\SerialColumn'],
            ['attribute' => 'id', 'visible' => false],
                        [
                'attribute' => 'question.question',
                'label' => 'Question'
            ],
            'status',
    ];
    echo Gridview::widget([
        'dataProvider' => $providerSessionJt,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-session-jt']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Session Jt'),
        ],
        'export' => false,
        'columns' => $gridColumnSessionJt
    ]);
}
?>

    </div>
</div>
