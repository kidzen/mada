<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model common\models\ResponseLookup */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Response Lookup', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="response-lookup-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= 'Response Lookup'.' '. Html::encode($this->title) ?></h2>
        </div>
        <div class="col-sm-3" style="margin-top: 15px">
            
            <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Delete', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ])
            ?>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        [
            'attribute' => 'question.question',
            'label' => 'Question',
        ],
        'response',
        'positivity_level',
        'status',
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]);
?>
    </div>
    <div class="row">
        <h4>Question<?= ' '. Html::encode($this->title) ?></h4>
    </div>
    <?php 
    $gridColumnQuestion = [
        ['attribute' => 'id', 'visible' => false],
        'type',
        'section_id',
        'question',
        'status',
    ];
    echo DetailView::widget([
        'model' => $model->question,
        'attributes' => $gridColumnQuestion    ]);
    ?>
</div>
