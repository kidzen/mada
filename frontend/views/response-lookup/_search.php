<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\search\ResponseLookupSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="form-response-lookup-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

    <?= $form->field($model, 'question_id')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\common\models\Question::find()->orderBy('id')->asArray()->all(), 'id', 'question'),
        'options' => ['placeholder' => 'Choose Question'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>

    <?= $form->field($model, 'response')->textInput(['maxlength' => true, 'placeholder' => 'Response']) ?>

    <?= $form->field($model, 'positivity_level')->textInput(['placeholder' => 'Positivity Level']) ?>

    <?= $form->field($model, 'status')->dropDownList([0=>'Inactive',10=>'Active']) ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
