<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\ParentTable */

$this->title = 'Create Parent Table';
$this->params['breadcrumbs'][] = ['label' => 'Parent Table', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="parent-table-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
