<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model common\models\ParentTable */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Parent Table', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="parent-table-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= 'Parent Table'.' '. Html::encode($this->title) ?></h2>
        </div>
        <div class="col-sm-3" style="margin-top: 15px">
            
            <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Delete', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ])
            ?>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        'name',
        'description',
        'status',
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]);
?>
    </div>
    
    <div class="row">
<?php
if($providerChildTable->totalCount){
    $gridColumnChildTable = [
        ['class' => 'yii\grid\SerialColumn'],
            ['attribute' => 'id', 'visible' => false],
                        'name',
            'description',
            'status',
    ];
    echo Gridview::widget([
        'dataProvider' => $providerChildTable,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-child-table']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Child Table'),
        ],
        'export' => false,
        'columns' => $gridColumnChildTable
    ]);
}
?>

    </div>
</div>
