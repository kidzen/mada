<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model common\models\User */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'User', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= 'User'.' '. Html::encode($this->title) ?></h2>
        </div>
        <div class="col-sm-3" style="margin-top: 15px">
            
            <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Delete', ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ])
            ?>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        'remote_ip',
        'mobile_id',
        'status',
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]);
?>
    </div>
    
    <div class="row">
<?php
if($providerPersonelInformation->totalCount){
    $gridColumnPersonelInformation = [
        ['class' => 'yii\grid\SerialColumn'],
            ['attribute' => 'id', 'visible' => false],
                        'department',
            'gender',
            'age',
            'service_duration',
            'status',
    ];
    echo Gridview::widget([
        'dataProvider' => $providerPersonelInformation,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-personel-information']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Personel Information'),
        ],
        'export' => false,
        'columns' => $gridColumnPersonelInformation
    ]);
}
?>

    </div>
    
    <div class="row">
<?php
if($providerResponse->totalCount){
    $gridColumnResponse = [
        ['class' => 'yii\grid\SerialColumn'],
            ['attribute' => 'id', 'visible' => false],
            [
                'attribute' => 'session.name',
                'label' => 'Session'
            ],
                        [
                'attribute' => 'question.question',
                'label' => 'Question'
            ],
            'response',
            'status',
    ];
    echo Gridview::widget([
        'dataProvider' => $providerResponse,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-response']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode('Response'),
        ],
        'export' => false,
        'columns' => $gridColumnResponse
    ]);
}
?>

    </div>
</div>
