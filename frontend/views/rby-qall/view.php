<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model common\models\RByQAll */

$this->title = $model->session;
$this->params['breadcrumbs'][] = ['label' => 'Rby Qall', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="rby-qall-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= 'Rby Qall'.' '. Html::encode($this->title) ?></h2>
        </div>
        <div class="col-sm-3" style="margin-top: 15px">
            
            <?= Html::a('Update', ['update', ], ['class' => 'btn btn-primary']) ?>
            <?= Html::a('Delete', ['delete', ], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => 'Are you sure you want to delete this item?',
                    'method' => 'post',
                ],
            ])
            ?>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        'session',
        'question',
        'avg_response',
        'percent',
        'no_responden',
        'qid',
        'sid',
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]);
?>
    </div>
</div>
