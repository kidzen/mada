<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\RByQAll */

$this->title = 'Create Rby Qall';
$this->params['breadcrumbs'][] = ['label' => 'Rby Qall', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="rby-qall-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
