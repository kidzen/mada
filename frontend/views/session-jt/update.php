<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\SessionJt */

$this->title = 'Update Session Jt: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Session Jt', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="session-jt-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
