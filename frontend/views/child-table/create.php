<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\ChildTable */

$this->title = 'Create Child Table';
$this->params['breadcrumbs'][] = ['label' => 'Child Table', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="child-table-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
